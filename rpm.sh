
########### VARIABLES ######################
_installer="yum"
_pkgs=(epel-release git httpd python3-pip)
user_site_name="flask_app"

validation(){
    logger $1 $?
}

install_tools(){
    for _pkg in ${_pkgs[@]}; do
        $_installer install -y $_pkg
    done
    validation "${FUNCNAME[0]}"
}
set_selinux(){
    sed -i 's/=enforcing/=permissive/g' /etc/selinux/config
    setenforce permissive
}
get_app_from_git(){
    git clone https://gitlab.com/mondbev/deploy_flask_with_apache.git 
    validation "${FUNCNAME[0]}"   
}

init_app(){
    cd deploy_flask_with_apache
    mv flask_app /var/www/ #moving the flask app dir to root dir
    mkdir /var/www/logs #creating dir for the logs
    cd /var/www/flask_app
    pip3 install -r requirements.txt
    validation "${FUNCNAME[0]}"
}


conf_apache(){
    echo "127.0.0.1 www.$user_site_name" >> /etc/hosts
    mv /etc/httpd/conf/httpd.conf{,orig}
    echo -n "
    ServerRoot \"/etc/httpd\"
    
    Listen 80
    
    Include conf.modules.d/*.conf
    
    User apache
    Group apache
    
    ServerAdmin root@$user_site_name
    ServerName www.$user_site_name:80
    
    <Directory />
        AllowOverride none
        Require all denied
    </Directory>
    

    <Directory "/var/www/$user_site_name">
        Options FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
 <IfModule dir_module>
     DirectoryIndex index.html index.php index.cgi
 </IfModule>

 <Files ".ht*">
     Require all denied
 </Files>
LogLevel warn

<IfModule alias_module>
 ScriptAlias /cgi-bin/ "/var/www/cgi-bin/" 
</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
 TypesConfig /etc/mime.types
 AddType application/x-compress .Z
 AddType application/x-gzip .gz .tgz
 AddType text/html .shtml
 AddOutputFilter INCLUDES .shtml
</IfModule>
 
AddDefaultCharset UTF-8

<IfModule mime_magic_module>
 MIMEMagicFile conf/magic
</IfModule>

IncludeOptional conf.d/*.conf
IncludeOptional sites-enabled/*.conf
    " > /etc/httpd/conf/httpd.conf 
    apachectl -t
    validation "${FUNCNAME[0]}" 
    systemctl enable --now httpd
}


config_virtual_host(){
    mkdir /etc/httpd/sites-available /etc/httpd/sites-enabled
    echo -n "
    #### configuration for the host, apache needs to know it is virtual ########

<VirtualHost *:80>
    ServerName www.$user_site_name
    ServerAlias $user_site_name
    DocumentRoot /var/www/$user_site_name
    ErrorLog /var/www/logs/$user_site_name-error.log
    CustomLog /var/www/logs/$user_site_name-requests.log combined
</VirtualHost>
    " > /etc/httpd/sites-available/$user_site_name.conf

    ln -s /etc/httpd/sites-available/$user_site_name.conf /etc/httpd/sites-enabled/$user_site_name.conf
    rm /etc/httpd/conf.d/welcome.conf
    systemctl restart httpd
    validation "${FUNCNAME[0]}" 
}

config_wsgi(){
    $_installer install -y python3-mod_wsgi
    validation "${FUNCNAME[0]}"
}
systemd_conf(){
    echo -n "
[Unit]
Description=Flask Daemon
After=network.target


[Service]
User=apache
Group=apache
WorkingDirectory=/var/www/$user_site_name
## for virt env this is the default path!!!!!!
ExecStart=/usr/local/bin/gunicorn  --workers 3 --bind unix:/opt/app/app.sock app:app 

[Install]
WantedBy=multi-user.target  
" > /etc/systemd/system/flask.service
systemctl daemon-reload
sleep 2
systemctl enable --now flask 
validation "${FUNCNAME[0]}"
}

check_all(){
    curl www.$user_site_name
}
########### MAIN FUNC ########

install_tools
set_selinux
get_app_from_git
conf_apache
init_app
config_virtual_host
config_wsgi
systemd_conf
check_all
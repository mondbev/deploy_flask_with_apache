#!/usr/bin/env bash
###############################
#owner: Shai. fork of droritzz
#purpose: automation for deployment flask with apache
#date: 8.3.21
#version: v.0.0.1
##############################


############### FUNCTIONS ###################

start_sel(){
echo "Please select package manager"

	select option in "deb" "rpm"; 
	 do
		case $option in
			deb) source $option.sh; break;;
			rpm) source $option.sh; break;;	
			*) start_sel; break;;
		esac
	 done
}
start_sel
exit 0

